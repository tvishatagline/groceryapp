import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import AppNavigation from "./src/navigation/AppNavigation";
import { GoogleSignin } from "@react-native-google-signin/google-signin";
import { LogBox } from 'react-native';
import { store } from "./src/redux/store/store";
import { Provider } from "react-redux";

LogBox.ignoreLogs([
  "[react-native-gesture-handler] Seems like you\'re using an old API with gesture components, check out new Gestures system!",
]);

GoogleSignin.configure({
  webClientId: '909518937279-2hl9sn5e5ffg4uef8pfm9c3mja24l5vi.apps.googleusercontent.com',
  iosClientId: '909518937279-onp9j0sta3ld8b3knptbuk9m3hci9vcp.apps.googleusercontent.com'
});

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <AppNavigation />
      </NavigationContainer>
    </Provider>
  );
}
  
export default App;
