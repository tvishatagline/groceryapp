
import { StyleSheet } from "react-native";
import { colors } from "../utils/colors";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFValue } from "react-native-responsive-fontsize";
import { fonts } from "../utils/fonts";

export const GlobalStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.GREEN,
    },
    logo_image: {
        height: wp(40),
        width: wp(40),
        resizeMode: 'contain',
    },
    shadowTopBtnView: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginBottom: hp(3),
    },
    subtitleText: {
        fontFamily: fonts.ROBOTO_LIGHT,
        fontSize: RFValue(16),
        color: colors.GREY,
        marginBottom: hp(4),
    },
    titleText: {
        fontSize: RFValue(24),
        fontFamily: fonts.ROBOTO_BOLD
    },
    TextInput: {
        fontSize: RFValue(16),
        paddingVertical: wp(4),
        borderBottomWidth: 1,
        borderBottomColor: colors.GREY,
        fontFamily: fonts.ROBOTO_MEDIUM
    },
    shadowTopBtnText: {
        fontSize: RFValue(17),
        color: colors.GREY,
        fontFamily: fonts.ROBOTO_MEDIUM
    },
    safeAreaView: {
        flex: 1,
        backgroundColor: colors.WHITE
    },
    signIn_button: {
        marginLeft: wp(5),
        backgroundColor: colors.GREEN,
        width: wp(25),
        height: wp(10),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5
    },
    errorMessage: {
        color: 'red',
        marginVertical: wp(2)
    },
    weightStyle: {
        color: 'grey',
        backgroundColor: colors.LIGHT_GREY,
        height: '90%',
        width: '34%',
        padding: '3.5%',
        justifyContent: 'center',
        marginBottom: '2%',
        textAlign: 'center', borderRadius: 5
    },
    flatlist: {
        height: wp(53),
        width: wp(45),
        backgroundColor: 'white',
        marginHorizontal: wp(2),
        marginTop: wp(3),
        borderRadius: 10,
        marginBottom: wp(2),
        padding: wp(3)
    },
    flatlistView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    flatlistSource: {
        height: hp(12),
        width: wp(100),
        alignItems: 'center',
        resizeMode: 'contain'
    },
    flatlistPrice: {
        fontSize: RFValue(12),
        fontFamily: fonts.ROBOTO_REGULAR
    },
    GroceryText: {
        fontFamily: fonts.ROBOTO_MEDIUM,
        fontSize: RFValue(14),
        marginLeft: 10,
        marginVertical: 10,
    },
    appBarStyle: {
        height: hp(5),
        backgroundColor: colors.GREEN,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    appBarText: {
        color: "white",
        fontSize: RFValue(20),
        fontFamily: fonts.ROBOTO_MEDIUM,
        marginLeft: wp(5)
    },
    shadowView: {
        backgroundColor: 'white',
        shadowColor: colors.GREY,
        shadowOffset: {
            width: 0,
            height: 8,
        },
        shadowOpacity: 0.26,
        shadowRadius: 5,
        elevation: 16,
        padding: wp(5),
        borderBottomRightRadius: wp(5),
        borderBottomLeftRadius: wp(5)
    }
});