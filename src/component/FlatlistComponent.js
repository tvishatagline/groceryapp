import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native'
import React from 'react'
import { GlobalStyles } from '../styles/style'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFValue } from "react-native-responsive-fontsize";
import { imagePaths } from '../utils/imagePaths';
import { fonts } from '../utils/fonts';
import { colors } from '../utils/colors';

const FlatlistComponent = ({ onPress, onClick, weight, isSelected, source, title, price }) => {
    return (
        <TouchableOpacity style={[styles.flatlistView, GlobalStyles.flatlist]} onPress={onPress}>
            <View style={styles.flatlistFavView}>
                <Text style={styles.weightText}>{weight}</Text>
                <TouchableOpacity onPress={onClick}>
                    <Image style={styles.fav} source={isSelected ? imagePaths.filled_img : imagePaths.without_filled_img}></Image>
                </TouchableOpacity>
            </View>
            <Image source={source} style={styles.flatImage}></Image>
            <View>
                <Text style={styles.flatlistTitle}>{title}</Text>
                <Text style={GlobalStyles.flatlistPrice}>{price}.00</Text>
            </View>
        </TouchableOpacity>
    )
}

export default FlatlistComponent;

const styles = StyleSheet.create({
    flatlistView: {
        marginHorizontal: wp(5)
    },
    flatlistFavView: {
        flexDirection: 'row',
        justifyContent: "space-between"
    },
    weightText: {
        backgroundColor: colors.LIGHT_GREY,
        color: 'grey',
        alignSelf: 'center',
        padding: wp(1)
    },
    flatImage: {
        margin: 10, height: hp(12),
        width: wp(30),
        resizeMode: 'contain'
    }, flatlistTitle: {
        fontSize: RFValue(14),
        fontFamily: fonts.ROBOTO_REGULAR,
        color: colors.GREY
    },

})