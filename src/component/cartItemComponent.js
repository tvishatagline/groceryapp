import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native'
import React, { useEffect, useState } from 'react'
import { imagePaths } from '../utils/imagePaths';
import { colors } from '../utils/colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Icon } from 'react-native-elements';
import { fonts } from '../utils/fonts';
import { RFValue } from 'react-native-responsive-fontsize';

const cartItemComponent = ({ item, index, onPress, increment, decrement, newData }) => {
    const [count, setCount] = useState(item.quantity);

    useEffect(() => {
        console.log(item);
    }, [])

    const handleQuantity = async (countPlus) => {
        const getData = await AsyncStorage.getItem('cartData')
        const storeData = JSON.parse(getData)
        let data = [...storeData]
        data[index].quantity = countPlus
        console.log({ data: data[index].quantity })

        await AsyncStorage.setItem('cartData', JSON.stringify(data))
        console.log("data", data);
        newData(data)
    }

    return (
        <View style={styles.mainView}>
            <View style={{ alignItems: 'center' }}>
                <Image source={item.source} style={styles.image}></Image>
            </View>
            <View style={{ flex: 1 }}>

                <Text style={styles.itemTitle}>{item.title}</Text>
                <Text style={styles.itemText}>weight: {item.weight}</Text>

                <Text style={styles.itemText}>price: {item.price}.00</Text>
                <Text style={styles.itemText}>Total Price:{item.price * count}.00 </Text>
                <View style={styles.buttonView}>
                    <Text style={styles.quantityText}>quantity: </Text>
                    <TouchableOpacity onPress={() => {
                        if (count > 1) {
                            let countPlus = count - 1
                            setCount(countPlus)
                            handleQuantity(countPlus)
                            decrement()
                        }
                    }}>
                        <View style={styles.button}>
                            <Image source={imagePaths.minus_img}></Image>
                        </View>
                    </TouchableOpacity>
                    <Text style={styles.itemQuantityText}>{count}</Text>
                    <TouchableOpacity
                        onPress={() => {
                            let countPlus = count + 1
                            setCount(countPlus),
                                handleQuantity(countPlus)
                            increment()
                        }} >
                        <View style={styles.button}>
                            <Image source={imagePaths.plus_img}></Image>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={onPress} >
                        <Icon name="trash" size={30} type="ionicon" />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default cartItemComponent


const styles = StyleSheet.create({
    appIconClose: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    safeAreaView: {
        backgroundColor: colors.GREEN
    },
    item_separator: {
        height: 2,
        backgroundColor: colors.LIGHT_GREY
    },
    button: {
        height: wp(7),
        width: wp(7),
        backgroundColor: colors.GREY,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    mainView: {
        flexDirection: 'row',
        alignItems: "center",
        padding: wp(2),
    },
    image: {
        width: wp(40),
        height: hp(15),
        resizeMode: 'contain',
        marginHorizontal: 10
    },
    itemTitle: {
        fontFamily: fonts.ROBOTO_MEDIUM,
        fontSize: RFValue(15),
        marginVertical: 5
    },
    itemText: {
        fontSize: RFValue(15),
        marginVertical: 5
    },
    quantityText: {
        fontSize: RFValue(15),
    },
    itemQuantityText: {
        width: 40,
        fontSize: RFValue(15),
        textAlign: "center"
    },
    buttonView: {
        flexDirection: 'row',
        marginVertical: 5,
        alignItems: 'center'
    }
})
