import { Text, TouchableOpacity } from 'react-native'
import React from 'react'
import { GlobalStyles } from '../styles/style'
import { colors } from '../utils/colors'

const ButtonComponent = ({ name, onPress }) => {
  return (
    <TouchableOpacity style={GlobalStyles.signIn_button} onPress={onPress}>
      <Text style={{ color: colors.WHITE }}>{name}</Text>
    </TouchableOpacity>
  )
}
export default ButtonComponent;