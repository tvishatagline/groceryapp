import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import SplashScreen from '../screens/SplashScreen'
import SignInScreen from '../screens/SignInScreen'
import SignUpScreen from '../screens/SignUpScreen'
import AddNumberScreen from '../screens/AddNumberScreen'
import VerifyNumberScreen from '../screens/VerifyNumberScreen'
import ForgotPassScreen from '../screens/ForgotPassScreen'
import DialogScreen from '../screens/DialogScreen'
import { colors } from '../utils/colors'

const Stack = createNativeStackNavigator()

const AuthStack = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Splash" component={SplashScreen} options={{ headerShown: false }} />
            <Stack.Screen name="SignIn" component={SignInScreen} options={{ headerShown: false }} />
            <Stack.Screen name="SignUp" component={SignUpScreen} options={{ headerShown: false }} />
            <Stack.Screen name="AddNumber" component={AddNumberScreen} options={{
                headerStyle: {
                    backgroundColor: colors.GREEN,
                }, headerTintColor: colors.WHITE,
            }} />
            <Stack.Screen name="VerifyNumber" component={VerifyNumberScreen} options={{
                headerStyle: {
                    backgroundColor: colors.GREEN,
                }, headerTintColor: colors.WHITE,
            }} />

            <Stack.Screen name="ForgotPass" component={ForgotPassScreen} options={{
                headerStyle: {
                    backgroundColor: colors.GREEN,
                }, headerTintColor: colors.WHITE,
            }} />
            <Stack.Screen name="Dialog" component={DialogScreen} options={{ headerShown: false }} />
        </Stack.Navigator>
    )
}

export default AuthStack