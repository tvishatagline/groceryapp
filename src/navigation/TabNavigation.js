import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { SafeAreaView } from 'react-native-safe-area-context';
import HomeScreen from '../screens/HomeScreen';
import { Appbar } from "react-native-paper";
import { useNavigation } from '@react-navigation/native';
import ShoppingScreen from '../screens/ShoppingScreen';
import FavoriteScreen from '../screens/FavoriteScreen';
import ProfileScreen from '../screens/ProfileScreen';
import { GlobalStyles } from '../styles/style';
import { colors } from '../utils/colors';

const Tab = createMaterialTopTabNavigator();

const TabNavigation = () => {
    const navigation = useNavigation();
    return (
        <>
            <SafeAreaView edges={['top']} style={{ backgroundColor: colors.GREEN }}>
                <View style={GlobalStyles.appBarStyle}>
                    <View style={styles.viewApp}>
                        <Appbar.Action icon='menu' color="white" onPress={() => navigation.goBack()} />
                        <Text style={GlobalStyles.appBarText}>Store</Text>
                    </View>
                    <View style={styles.viewAppfirst}>
                        <Appbar.Action icon="cart" onPress={() => navigation.navigate('AddToCart')} color="white" marginLeft='' />
                        <Appbar.Action icon="bell" color="white" onPress={() => navigation.navigate('Notification')} />
                    </View>
                </View>
            </SafeAreaView>
            <Tab.Navigator
                screenOptions={{
                    tabBarStyle: {
                        backgroundColor: colors.GREEN,

                    }, tabBarIndicatorStyle: { backgroundColor: colors.WHITE }
                }}>
                <Tab.Screen
                    name="Home"
                    component={HomeScreen} options={{
                        tabBarIcon: ({ focused }) => (
                            <View>
                                <FontAwesome5Icon
                                    name={'store'}
                                    size={17}
                                    color={focused ? colors.WHITE : colors.PRIMARY_GREEN}
                                />
                            </View>
                        ), tabBarShowLabel: false,
                    }}
                />
                <Tab.Screen name="Shopping" component={ShoppingScreen} options={{
                    tabBarIcon: ({ focused }) => (
                        <View>
                            <FontAwesome5Icon
                                name={'shopping-basket'}
                                size={18}
                                color={focused ? colors.WHITE : colors.PRIMARY_GREEN}
                            />
                        </View>
                    ), tabBarShowLabel: false,
                }} />
                <Tab.Screen name="Favorite" component={FavoriteScreen} options={{
                    tabBarIcon: ({ focused }) => (
                        <View>
                            <FontAwesome5Icon
                                name={'heart'}
                                size={18}
                                color={focused ? colors.WHITE : colors.PRIMARY_GREEN}
                            />
                        </View>
                    ), tabBarShowLabel: false,
                }} />
                <Tab.Screen name="Person" component={ProfileScreen} options={{
                    tabBarIcon: ({ focused }) => (
                        <View>
                            <FontAwesome5Icon
                                name={'user-circle'}
                                size={18}
                                color={focused ? colors.WHITE : colors.PRIMARY_GREEN}
                            />
                        </View>
                    ), tabBarShowLabel: false,
                }} />
            </Tab.Navigator>
        </>
    );
}
export default TabNavigation;
const styles = StyleSheet.create({
    viewApp: {
        flexDirection: 'row', alignItems: 'center'
    }, viewAppfirst: {
        flexDirection: 'row'
    }
})