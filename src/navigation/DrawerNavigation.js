import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import CustomDrawer from '../screens/CustomDrawer';
import HomeStack from './HomeStack';

const Drawer = createDrawerNavigator();

const DrawerNavigation = () => {
  return (
    <Drawer.Navigator drawerContent={(props) => <CustomDrawer {...props} />}>
      <Drawer.Screen name='HomeStack' component={HomeStack} options={{ headerShown: false }} />
    </Drawer.Navigator>
  );
}

export default DrawerNavigation;
