import React from 'react'
import AuthStack from './AuthStack'
import DrawerNavigation from './DrawerNavigation'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import HomeStack from './HomeStack'

const Stack = createNativeStackNavigator()

const AppNavigation = () => {
    return (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
            <Stack.Screen name='AuthStack' component={AuthStack} />
            <Stack.Screen name='DrawerStack' component={HomeStack} />
        </Stack.Navigator>
    )
}

export default AppNavigation