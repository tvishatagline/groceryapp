import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import TabNavigation from './TabNavigation'
import HomeScreen from '../screens/HomeScreen'
import NotificationScreen from '../screens/NotificationScreen'
import CoffeeScreen from '../screens/CoffeeScreen'
import BeveragesScreen from '../screens/BeveragesScreen'
import GroceryAppDealsScreen from '../screens/GroceryAppDealsScreen'
import AddToCartScreen from '../screens/AddToCartScreen'

const Stack = createNativeStackNavigator()

const HomeStack = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name='TabNavigation' component={TabNavigation} options={{ headerShown: false }} />
            <Stack.Screen name="Home" component={HomeScreen} options={{ headerShown: false }} />
            <Stack.Screen name="AddToCart" component={AddToCartScreen} options={{ headerShown: false }} />
            <Stack.Screen name='Notification' component={NotificationScreen} options={{ headerShown: false }} />
            <Stack.Screen name="CoffeeScreen" component={CoffeeScreen} options={{ headerShown: false }} />
            <Stack.Screen name="BeveragesScreen" component={BeveragesScreen} options={{ headerShown: false }} />
            <Stack.Screen name="AppDeals" component={GroceryAppDealsScreen} options={{ headerShown: false }} />
        </Stack.Navigator>
    )
}

export default HomeStack