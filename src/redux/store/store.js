// import { createStore, combineReducers, applyMiddleware } from 'redux';
// import thunk from 'redux-thunk';
// // import apiReducer from './redux/reducer/apiCallReducer';
// import changeNumber from './redux/reducer/reducer';
// const rootReducer = combineReducers({
//     changeNumber
// });

// // const rootReducer = combineReducers({
// //     apiReducer,
// // });
// export const store = createStore(rootReducer, applyMiddleware(thunk))

import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import apiReducer from '../reducer/reducer';
import { createLogger } from 'redux-logger';
const rootReducer = combineReducers({
    apiReducer,
});

const logger = createLogger();
export const store = createStore(rootReducer, applyMiddleware(thunk, logger));