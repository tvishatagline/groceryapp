// export const incNumber = () => {
//     return {
//         type: "INCREMENT"
//     }
// }
// export const decNumber = () => {
//     return {
//         type: "DECREMENT"
//     }
// }
// export const resetNumber = () => {
//     return {
//         type: "RESET"
//     }
// }

import axios from "axios"
import { API_ERROR,  API_PENDING, API_SUCCESS } from "../reducer/reducer";

export const fetchData = () => ({
    type: API_PENDING,
})

export const fetchSuccess = (data) => ({
    type: API_SUCCESS,
    payload: data,
})

export const fetchError = (error) => ({
    type: API_ERROR,
    payload: error,
})

export const getApiData = (url) => {

    return (dispatch) => {
        dispatch(fetchData())
        axios.get(url)
            .then((response) => {
                console.log({ response })
                if (response.status) {
                    dispatch(fetchSuccess(response.data))
                } else {
                   
                }
            })
            .catch((error) => {
                dispatch(fetchError(error))
                console.log(error)
            })
    }
}
