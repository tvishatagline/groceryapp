// const initialState = 0;

// const changeNumber = (state = initialState, action) => {
//     switch(action.type){
//         case "INCREMENT" : return state + 1;
//         case "DECREMENT" : return state - 1;
//         case "RESET" : return 0;
//         default : return state; 
//     }
// }

// export default changeNumber; 

export const API_SUCCESS = 'API_SUCCESS';
export const API_PENDING = 'API_PENDING';
export const API_ERROR = 'API_ERROR';

const initialState = {
    data: { info: {}, data: [] },
    error: [],
    loading: false,
};

const apiReducer = (state = initialState, action) => {
    switch (action.type) {
        case API_PENDING:
            return {
                ...state,
                loading: true,
            }
        case API_SUCCESS:
            let oldArr = state.data.data;
            console.log({ state, action });
            return {
                ...state,
                data: { info: action.payload.info, data: oldArr.concat(action.payload.results) },
                loading: false,
            }
        case API_ERROR:
            return {
                ...state,
                error: action.payload,
                loading: false,
            }
        default:
            return state;
    }
}

export default apiReducer;