import { View, Text, FlatList, Image, StyleSheet, ActivityIndicator, RefreshControl } from 'react-native'
import React, { useState, useEffect }  from 'react'
import axios from 'axios';
import { colors } from '../utils/colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFValue } from "react-native-responsive-fontsize";
import { fonts } from '../utils/fonts';

const FavoriteScreen = () => {
  const [data, setData] = useState();
  const [footerLoading, setFooterLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false)
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    setIsLoading(true)
    setTimeout(() => {
      getData(page)
    }, 2000)
  }, []);

  const onRefresh = () => {
    setRefresh(true)
    getData(1)
  }

  const getData = (page) => {
    setIsLoading(false);
    axios.get('https://rickandmortyapi.com/api/character/?page=' + page)
      .then((response) => {
        console.log({ response });
        setPage(page)
        setIsLoading(false)
        setFooterLoading(false)
        setRefresh(false)
        const myData = response.data.results;
        console.log({ myData });
        setData(page === 1 ? response.data.results : data.concat(response.data.results));
      })
      .catch((error) => {
        console.error(error);
      })
  }

  const renderFooter = () => {
    return (
      <View style={styles.footer}>
        {footerLoading ? (
          <ActivityIndicator color={colors.GREEN} style={{ margin: 15, justifyContent: "center" }} />
        ) : null}
      </View>
    );
  };

  return (
    <View style={styles.mainView}>
      {isLoading ? <ActivityIndicator 
        color={colors.GREEN}
        style={{
          justifyContent: "center",
          flex: 1
        }}
      /> : (
        <FlatList
          data={data}
          ItemSeparatorComponent={() => {
            return (<View style={styles.item_separator} />);
          }}
          renderItem={({ item }) => {
            return (
              <View style={{ flexDirection: 'row' }}>
                <View>
                  <Image source={{ uri: item.image }} style={styles.flatImage}></Image>
                </View>
                <View style={{ margin: wp(2) }}>
                  <Text>{item.id}</Text>
                  <Text style={styles.flatText}>Name: {item.name}</Text>
                  <Text>Gender: {item.gender}</Text>
                  <Text>Status: {item.status}</Text>
                </View>
              </View>
            )
          }}
          onEndReached={() => getData(page + 1)}
          onEndReachedThreshold={0.5}
          ListFooterComponent={renderFooter}
          refreshControl={
            <RefreshControl
              refreshing={refresh}
              onRefresh={onRefresh}
              progressBackgroundColor={colors.GREEN}
              tintColor={colors.GREEN}
            />
          }
        />
      )}
    </View >
  )
}

export default FavoriteScreen;

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    padding: wp(2)
  },
  item_separator: {
    height: 2,
    backgroundColor: colors.LIGHT_GREY
  },
  flatText: {
    fontFamily: fonts.ROBOTO_MEDIUM,
    fontSize: RFValue(15)
  },
  flatImage: {
    height: hp(10),
    width: wp(20)
  },
  footer: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
})