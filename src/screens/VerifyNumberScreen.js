import React from "react";
import { SafeAreaView, StyleSheet, Text, View } from "react-native";
import { en } from "../utils/en";
import { GlobalStyles } from "../styles/style";
import { useNavigation } from "@react-navigation/native";
import ButtonComponent from "../component/ButtonComponent";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFValue } from "react-native-responsive-fontsize";
import OTPInputView from "@twotalltotems/react-native-otp-input";
import { colors } from "../utils/colors";
import { fonts } from "../utils/fonts";

const VerifyNumberScreen = ({ }) => {
    const navigation = useNavigation();
    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={GlobalStyles.shadowView}>
                <Text style={GlobalStyles.titleText}>{en.VERIFY_NUMBER}</Text>
                <Text style={GlobalStyles.subtitleText}>{`4 digit code sent to`}</Text>
                <View style={styleSheet.view}>
                    <OTPInputView
                        style={styleSheet.OtpInputView}
                        pinCount={4}
                        autoFocusOnLoad
                        codeInputFieldStyle={styleSheet.underlineStyleBase}
                        codeInputHighlightStyle={styleSheet.underlineStyleHighLighted}
                    />
                </View>
                <View style={styleSheet.textView}>
                    <Text style={styleSheet.resendText}>Resend </Text>
                        <ButtonComponent name="VERIFY" onPress={() => navigation.navigate("Dialog")} />
                </View>
            </View>
        </SafeAreaView>
    );
};

const styleSheet = StyleSheet.create({
    borderStyleBase: {
        width: 30,
        height: 45,
        borderBottomColor: 'black'
    },
    borderStyleHighLighted: {
        borderColor: colors.GREY,
    },
    underlineStyleBase: {
        width: 30,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 1,
        borderBottomColor: 'black'
    },
    underlineStyleHighLighted: {
        borderColor: "black",
    },  resendText: {
        fontSize: RFValue(16),
        color: colors.GREY,
        fontFamily:fonts.ROBOTO_LIGHT
    }, textView: {
        marginTop: wp(8),
        flexDirection: 'row',
        justifyContent: 'space-between'
    }, OtpInputView: {
        width: wp(80),
        height: hp(5),
    }
});

export default VerifyNumberScreen;