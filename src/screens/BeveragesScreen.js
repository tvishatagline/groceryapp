import { View, Image, FlatList, TouchableOpacity, SafeAreaView, Text, StyleSheet } from 'react-native'
import React from 'react'
import { useNavigation } from '@react-navigation/native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFValue } from "react-native-responsive-fontsize";
import { colors } from '../utils/colors';
import { imagePaths } from '../utils/imagePaths';
import { fonts } from '../utils/fonts';
const Data = [
    {
        id: 1,
        name: "Chocolate drink"
    },
    {
        id: 2,
        name: "Coffee"
    },
    {
        id: 3,
        name: "Concentrated Fruit Drinks"
    },
    {
        id: 4,
        name: "Juices & Carbonates"
    },
    {
        id: 5,
        name: "Malt Drink"
    },
    {
        id: 6,
        name: "RTD & Creamers"
    },
    {
        id: 7,
        name: "Sport & Energy Drinks"
    },
    {
        id: 8,
        name: "Tea"
    },
    {
        id: 9,
        name: "Waters"
    }
];

const BeveragesScreen = () => {
    const navigation = useNavigation();
    const newScreen = (item) => {
        let id = item.id
        switch (id) {
            case 2:
                navigation.navigate('CoffeeScreen')
                break;
            default:
                navigation.navigate('BeveragesScreen')
        }
    }
    return (
        <View style={{ flex: 1 }} >
            <Image source={imagePaths.cold_dirnks_img} style={styles.beverages_image}></Image>
            <SafeAreaView style={styles.beverages_safeAreaView}>
                <TouchableOpacity onPress={() => navigation.navigate("DrawerStack")}>
                    <Image source={imagePaths.arrow_img} />
                </TouchableOpacity>
                <TouchableOpacity>
                    <Image source={imagePaths.magnify_img} />
                </TouchableOpacity>
            </SafeAreaView>
            <View style={styles.main_view_flat}>
                <View style={styles.view_flat}>
                    <FlatList
                        data={Data}
                        scrollEnabled={false}
                        ItemSeparatorComponent={(props) => {
                            return (<View style={styles.item_separator} />);
                        }}
                        style={styles.flat_style}
                        renderItem={({ item }) => {
                            return (
                                <>
                                    <TouchableOpacity onPress={() => { newScreen(item) }}>
                                        <View style={styles.view_style}>
                                            <Text style={styles.text_style}>{item.name}</Text>
                                            <Image source={imagePaths.smallArrow_img} style={styles.image_small} ></Image>
                                        </View>
                                    </TouchableOpacity>
                                </>
                            )
                        }
                        }
                    />
                </View>
            </View>
        </View >
    )
}
export default BeveragesScreen;
const styles = StyleSheet.create({
    beverages_image: {
        height: hp(25),
        width: wp(100)
    },
    beverages_safeAreaView: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        position: 'absolute',
        width: '100%',
        marginLeft: '5%',
        marginRight: '20%'
    },
    main_view_flat: {
        flex: 1,
        position: 'absolute'
    },
    view_flat: {
        marginVertical: hp(20),
    },
    item_separator: {
        height: 2,
        backgroundColor: colors.LIGHT_GREY
    },
    flat_style: {
        marginLeft: wp(5),
        marginRight: wp(5),
        borderRadius: 5,
    },
    view_style: {
        backgroundColor: 'white',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: wp(90),
        borderRadius: 5,
    },
    text_style: {
        padding: hp(2),
        fontSize: RFValue(16),
        fontFamily: fonts.ROBOTO_MEDIUM
    },
    image_small: {
        marginTop: wp(5),
        marginRight: wp(5)
    }
})