import { View, Text, SafeAreaView, FlatList, Image, StyleSheet } from 'react-native'
import React, { useState } from 'react'
import { Appbar } from "react-native-paper";
import { useNavigation } from '@react-navigation/native';
import FlatlistComponent from '../component/FlatlistComponent';
import { colors } from '../utils/colors';
import { imagePaths } from '../utils/imagePaths';
import { GlobalStyles } from '../styles/style';

const CoffeeData = [
    {
        id: 1,
        weight: '50G',
        source: imagePaths.coffee_img_first,
        title: 'Khao Shong Ag...',
        price: 470.00,
        isSelected: false,
        quantity: 1,
        categories: 'coffee',
    },
    {
        id: 2,
        weight: '50G',
        source: imagePaths.coffee_img_second,
        title: 'Khao Shong...',
        price: 450.00,
        isSelected: false,
        quantity: 1,
        categories: 'coffee',
    },
    {
        id: 3,
        weight: '100G',
        source: imagePaths.coffee_img_third,
        title: 'Nescafe Gold',
        price: 1700.00,
        isSelected: false,
        quantity: 1,
        categories: 'coffee',
    },
    {
        id: 4,
        weight: '50G',
        source: imagePaths.coffee_img_forth,
        title: 'Nescafe Classic',
        price: 470.00,
        isSelected: false,
        quantity: 1,
        categories: 'coffee',
    },
    {
        id: 5,
        weight: '100G',
        source: imagePaths.coffee_img_fifth,
        title: 'Nescafe Latte',
        price: 950.00,
        isSelected: false,
        quantity: 1,
        categories: 'coffee',
    }
];

const CoffeeScreen = () => {
    const navigation = useNavigation();
    const [coffee, setCoffee] = useState(CoffeeData)

    const click = (id) => {
        let updatedData = coffee.map((item) => {
            if (item.id == id) {
                return { ...item, isSelected: !item.isSelected }
            } else {
                return item
            }
        })
        setCoffee(updatedData)
    }

    return (
        <View>
            <SafeAreaView edges={['top']} style={styles.coffee_view}>
                <View style={GlobalStyles.appBarStyle}>
                    <View style={styles.coffee_app_view}>
                        <Appbar.BackAction color="white" onPress={() => navigation.navigate('BeveragesScreen')} />
                        <Text style={GlobalStyles.appBarText}>Coffee</Text>
                    </View>
                    <Appbar.Action icon="magnify" color="white" />
                </View>
            </SafeAreaView>
            <View>
                <FlatList
                    contentContainerStyle={styles.coffee_flat_list}
                    data={coffee}
                    renderItem={({ item, index }) => {
                        return (
                            <FlatlistComponent
                                weight={item.weight}
                                isSelected={item.isSelected}
                                source={item.source}
                                title={item.title}
                                price={item.price}
                                onPress={() => { navigation.navigate("AppDeals", { data: item }) }} onClick={() => click(item.id)} />
                        )
                    }}
                />
            </View>
        </View>
    )
}

export default CoffeeScreen;
const styles = StyleSheet.create({
    coffee_view: {
        backgroundColor: colors.GREEN
    }, coffee_app_view: {
        flexDirection: 'row',
        alignItems: 'center'
    }, coffee_flat_list: {
        flexDirection: "row",
        flexWrap: 'wrap'
    }
})