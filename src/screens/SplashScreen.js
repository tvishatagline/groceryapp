import { useNavigation } from "@react-navigation/native";
import React from "react";
import { Image, SafeAreaView, Text, TouchableOpacity, StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { GlobalStyles } from "../styles/style";
import { colors } from "../utils/colors";
import { en } from "../utils/en";
import { imagePaths } from "../utils/imagePaths";
import { RFValue } from "react-native-responsive-fontsize";
import { fonts } from "../utils/fonts";

const SplashScreen = () => {
    const navigation = useNavigation();
    return (
        <SafeAreaView style={[GlobalStyles.container, styles.container]}>
            <Image source={imagePaths.splash_img} style={GlobalStyles.logo_image} />
            <Text style={styles.welcomeText}>Welcome to <Text style={{fontFamily:fonts.ROBOTO_BOLD}}>{'\n'}Grocery</Text> shopping</Text>
            <TouchableOpacity style={styles.btnStyle} onPress={() => navigation.navigate('SignIn')}>
                <Text style={styles.btnText}>{en.SIGNIN_BUTTON}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.btnStyle, styles.btnSignup]} onPress={() => navigation.navigate('SignUp')}>
                <Text style={[styles.btnText, styles.btnSignupText]}>{en.SIGNUP_BUTTON}</Text>
            </TouchableOpacity>
        </SafeAreaView>
    );
}

export default SplashScreen;

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    welcomeText: {
        color: colors.WHITE,
        fontSize: RFValue(32),
        textAlign: 'center',
        marginTop: hp(22),
        marginBottom: hp(5),
        fontFamily:fonts.ROBOTO_LIGHT
    },
    btnStyle: {
        alignItems: 'center',
        backgroundColor: colors.WHITE,
        borderRadius: 5,
        paddingVertical: wp(2.5),
        paddingHorizontal: wp(10),
        marginBottom: wp(2)
    },
    btnSignup: {
        marginBottom: wp(2),
        backgroundColor: colors.GREEN
    },
    btnText: {
        fontSize: RFValue(14),
        color: colors.GREEN,
        fontFamily:fonts.ROBOTO_REGULAR
    },
    btnSignupText: {
        color: colors.WHITE
    }
})