import { View, Text, FlatList, Image, StyleSheet, ActivityIndicator, RefreshControl } from 'react-native'
import React, { useEffect, useState } from 'react'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFValue } from "react-native-responsive-fontsize";
import { colors } from '../utils/colors';
import { fonts } from '../utils/fonts';

const ShoppingScreen = () => {
  const [data, setData] = useState([]);
  const [footerLoading, setFooterLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false)
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    setIsLoading(true)
    setTimeout(() => {
      getApiData(page)
    }, 2000)
  }, []);

  const onRefresh = () => {
    setRefresh(true)
    getApiData(1)
  }

  const getApiData = (page) => {
    setFooterLoading(true);
    fetch('https://rickandmortyapi.com/api/character/?page=' + page)
      .then((response) => response.json())
      .then((responseJson) => {
        setPage(page)
        setIsLoading(false)
        setFooterLoading(false)
        setRefresh(false)
        setData(page === 1 ? responseJson.results : data.concat(responseJson.results));
        console.log("data", data);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const renderFooter = () => {
    return (
      <View style={styles.footer}>
        {footerLoading ? (
          <ActivityIndicator color={colors.GREEN} style={{ margin: 15, justifyContent: "center" }} />
        ) : null}
      </View>
    );
  };

  return (
    <View style={{ flex: 1, padding: wp(2) }}>
      {isLoading ?
        <ActivityIndicator color={colors.GREEN}
          style={{
            justifyContent: "center",
            flex: 1
          }} /> : (
          <FlatList
            data={data}
            keyExtractor={({ id }) => id}
            ItemSeparatorComponent={() => {
              return (<View style={styles.item_separator} />);
            }}
            renderItem={({ item }) => {
              return (
                <View style={{ flexDirection: "row" }}>
                  <View>
                    <Image source={{ uri: item.image }} style={styles.image}></Image>
                  </View>
                  <View style={{ margin: wp(2) }}>
                    <Text>{item.id}</Text>
                    <Text style={styles.title}>Name: {item.name}</Text>
                    <Text>Gender: {item.gender}</Text>
                    <Text>Species: {item.species}</Text>
                    <Text>Status: {item.status}</Text>
                  </View>
                </View>
              )
            }}
            onEndReached={() => getApiData(page + 1)}
            onEndReachedThreshold={0.5}
            ListFooterComponent={renderFooter}
            refreshControl={
              <RefreshControl
                refreshing={refresh}
                onRefresh={onRefresh}
                progressBackgroundColor={colors.GREEN}
                tintColor={colors.GREEN}
              />
            }
          />
        )}
    </View>
  )
}

export default ShoppingScreen;

const styles = StyleSheet.create({
  item_separator: {
    height: 2,
    backgroundColor: colors.LIGHT_GREY
  },
  image: {
    height: hp(10),
    width: wp(20)
  },
  title: {
    fontFamily: fonts.ROBOTO_MEDIUM,
    fontSize: RFValue(15)
  },
  footer: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },

})