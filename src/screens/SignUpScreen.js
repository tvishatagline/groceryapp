import React, { useState } from "react";
import { Text, TouchableOpacity, View, TextInput, SafeAreaView, StyleSheet } from "react-native";
import { GlobalStyles } from "../styles/style";
import { en } from "../utils/en";
import { useNavigation } from "@react-navigation/native";
import ButtonComponent from "../component/ButtonComponent";
import * as yup from 'yup';
import { Formik } from 'formik';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { colors } from "../utils/colors";

const SignUpScreen = () => {
    const navigation = useNavigation();

    const loginValidationSchema = yup.object().shape({
        name: yup
            .string()
            .label('Name')
            .required('Please enter your name'),
        email: yup
            .string()
            .email("Please enter valid email")
            .required('Email Address is Required'),
        password: yup
            .string()
            .min(6, ({ min }) => `Password must be at least ${min} characters`)
            .required('Password is required'),
    })
    return (
        <SafeAreaView style={GlobalStyles.safeAreaView} edges={['top']}>
            <View style={GlobalStyles.shadowView}>
                <View style={GlobalStyles.shadowTopBtnView}>
                    <TouchableOpacity onPress={() => navigation.navigate('SignIn')}>
                        <Text style={GlobalStyles.shadowTopBtnText}>Sign in</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigation.navigate('SignUp')}>
                        <Text style={[GlobalStyles.shadowTopBtnText, {
                            color: colors.GREEN
                        }]}>Sign up</Text>
                    </TouchableOpacity>
                </View>
                <Text style={GlobalStyles.titleText}>{en.TEXT_SIGNUP}</Text>
                <Text style={GlobalStyles.subtitleText}>{en.TEXT_SIGNUP_START}</Text>
                <Formik
                    validationSchema={loginValidationSchema}
                    initialValues={{ name: '', email: '', password: '' }}
                    onSubmit={(values) => {
                        console.log(values)
                        navigation.navigate('AddNumber')
                    }}>
                    {({ handleChange, handleBlur, handleSubmit, values, errors }) => (
                        <>
                            <TextInput
                                style={GlobalStyles.TextInput}
                                placeholder="Username"
                                value={values.name}
                                onChangeText={handleChange('name')}
                                onBlur={handleBlur('name')}
                            />
                            {errors.name &&
                                <Text style={[GlobalStyles.errorMessage,]}>{errors.name}</Text>
                            }
                            <TextInput
                                style={GlobalStyles.TextInput}
                                placeholder="Email address"
                                value={values.email}
                                onChangeText={handleChange('email')}
                                onBlur={handleBlur('email')}
                            />
                            {errors.email &&
                                <Text style={GlobalStyles.errorMessage}>{errors.email}</Text>
                            }
                            <TextInput
                                style={GlobalStyles.TextInput}
                                placeholder="Password"
                                value={values.password}
                                secureTextEntry
                                onChangeText={handleChange('password')}
                                onBlur={handleBlur('password')}
                            />
                            {errors.password &&
                                <Text style={GlobalStyles.errorMessage}>{errors.password}</Text>
                            }
                            <View style={styleSheet.btn}>
                                <ButtonComponent name={en.SIGNUP_BUTTON} onPress={() => { handleSubmit(); }} />
                            </View>
                        </>
                    )}
                </Formik>
            </View>
        </SafeAreaView>


    );
}

export default SignUpScreen;

const styleSheet = StyleSheet.create({
    btn: {
        alignItems: 'flex-end',
        marginTop: hp(2)
    }
})