import { useNavigation } from "@react-navigation/native";
import React, { useState } from "react";
import { FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import FlatlistComponent from "../component/FlatlistComponent";
import { GlobalStyles } from "../styles/style";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFValue } from "react-native-responsive-fontsize";
import { colors } from "../utils/colors";
import { imagePaths } from "../utils/imagePaths";
import { en } from "../utils/en";
import { fonts } from "../utils/fonts";

const Data = [
    {
        id: 1,
        image: imagePaths.house_hold_img,
        title: 'Household'
    },
    {
        id: 2,
        image: imagePaths.grocery_img,
        title: 'Grocery'
    },
    {
        id: 3,
        image: imagePaths.liquor_img,
        title: 'Liquor'
    },
    {
        id: 4,
        image: imagePaths.chillied_img,
        title: 'Chilled'
    },
    {
        id: 5,
        image: imagePaths.beverages_img,
        title: 'Beverages',
    },
    {
        id: 6,
        image: imagePaths.pharmacy_img,
        title: 'Pharmacy'
    },
    {
        id: 7,
        image: imagePaths.frozen_food_img,
        title: 'Frozen food'
    },
    {
        id: 8,
        image: imagePaths.vegetables_img,
        title: 'Vegetables'
    },
    {
        id: 9,
        image: imagePaths.meat_img,
        title: 'Meat'
    },
    {
        id: 10,
        image: imagePaths.fish_img,
        title: 'Fish'
    },
    {
        id: 11,
        image: imagePaths.home_ware_img,
        title: 'Homeware'
    },
    {
        id: 12,
        image: imagePaths.fruits_img,
        title: 'Fruits'
    },
]

const vegetablesData = [
    {
        id: 1,
        weight: '100G',
        source: imagePaths.ginger_img,
        title: 'Ginger',
        price: 60.00,
        isSelected: false,
        quantity: 1,
        categories: 'vegetable',
    },
    {
        id: 2,
        weight: '100G',
        source: imagePaths.garlic_img,
        title: 'Garlic',
        price: 40.00,
        isSelected: false,
        quantity: 1,
        categories: 'vegetable',
    },
    {
        id: 3,
        weight: '1KG',
        source: imagePaths.onion_img,
        title: 'Onion',
        price: 260.00,
        isSelected: false,
        quantity: 1,
        categories: 'vegetable',
    },
    {
        id: 4,
        weight: '100G',
        source: imagePaths.ginger_img,
        title: 'Ginger',
        price: 60.00,
        isSelected: false,
        quantity: 1,
        categories: 'vegetable',
    },
    {
        id: 5,
        weight: '100G',
        source: imagePaths.garlic_img,
        title: 'Garlic',
        price: 40.00,
        isSelected: false,
        quantity: 1,
        categories: 'vegetable',
    },
    {
        id: 6,
        weight: '1KG',
        source: imagePaths.onion_img,
        title: 'Onion',
        price: 260.00,
        isSelected: false,
        quantity: 1,
        categories: 'vegetable',
    }
]

const FruitsData = [
    {
        id: 7,
        weight: '1KG',
        source: imagePaths.carrot_img,
        title: 'Carrot',
        price: 490.00,
        isSelected: false,
        quantity: 1,
        categories: 'fruit',
    },
    {
        id: 8,
        weight: '1KG',
        source: imagePaths.mango_img,
        title: 'Mango - Bud',
        price: 210.00,
        isSelected: false,
        quantity: 1,
        categories: 'fruit',
    },
    {
        id: 9,
        weight: '100G',
        source: imagePaths.grapes_img,
        title: 'Grapes - Green',
        price: 140.00,
        isSelected: false,
        quantity: 1,
        categories: 'fruit',
    },
    {
        id: 10,
        weight: '1KG',
        source: imagePaths.carrot_img,
        title: 'Carrot',
        price: '490.00',
        isSelected: false,
        quantity: 1,
        categories: 'fruit',
    },
    {
        id: 11,
        weight: '1KG',
        source: imagePaths.mango_img,
        title: 'Mango - Bud',
        price: 210.00,
        isSelected: false,
        quantity: 1,
        categories: 'fruit',
    },
    {
        id: 12,
        weight: '100G',
        source: imagePaths.grapes_img,
        title: 'Grapes - Green',
        price: 140.00,
        isSelected: false,
        quantity: 1,
        categories: 'fruit',
    },
]

const HomeScreen = () => {

    const [open, setOpen] = useState(false)
    const [vegetables, setVegetables] = useState(vegetablesData)
    const [fruits, setFruits] = useState(FruitsData)
    const navigation = useNavigation();

    const click = (id) => {
        // let cloneData = [...vegetables]
        // let isIndex = vegetables.findIndex((item) => item.id == id)
        // if (isIndex >= 0) {
        //     cloneData[isIndex].isSelected = !cloneData[isIndex].isSelected
        // }
        // let listData = type == 'veg' ? vegetables : fruits
        let updatedData = vegetables.map((item) => {
            if (item.id == id) {
                return { ...item, isSelected: !item.isSelected }
            } else {
                return item
            }
        })
        setVegetables(updatedData)
    }
  
    const clickOn = (id) => {
        let updatedData1 = fruits.map((item) => {
            if (item.id == id) {
                return { ...item, isSelected: !item.isSelected }
            } else {
                return item
            }
        })
        setFruits(updatedData1)
    }

    const newScreen = (item) => {
        let id = item.id
        switch (id) {
            case 5:
                navigation.navigate('BeveragesScreen')
                break;
            default:
                navigation.navigate('HomeScreen')
        }
    }

    return (
        <View style={{ marginBottom: 30 }}>
            <View style={GlobalStyles.shadowView}>
                <Text style={GlobalStyles.GroceryText}>{en.ALL_CATEGORIES}</Text>
                <FlatList
                    style={{ height: open ? null : 100 }}
                    data={Data}
                    scrollEnabled={false}
                    numColumns={4}
                    renderItem={({ item }) => {
                        return (
                            <View style={styles.home_first_view}>
                                <TouchableOpacity onPress={() => { newScreen(item) }}>
                                    <View style={styles.home_image_view}>
                                        <Image source={item.image} style={styles.home_image} />
                                    </View>
                                </TouchableOpacity>
                                <Text style={styles.home_text}>{item.title}</Text>
                            </View>
                        )
                    }}
                />
            </View>

            <TouchableOpacity style={[styles.down_image_view]}
                onPress={() => { setOpen(!open) }}>
                <Image source={imagePaths.down_arrow_img} style={styles.down_image} />
            </TouchableOpacity>

            <View style={styles.veg_view}>
                <Text style={GlobalStyles.GroceryText}>{en.GROCERY_APP_MEMBER_DEALS}</Text>
                <Text style={styles.text_viewAll}>{en.VIEW_ALL}</Text>
                <Image source={imagePaths.smallArrow_img} style={styles.image_arrow}></Image>
            </View>

            <FlatList
                data={vegetables}
                horizontal
                renderItem={({ item }) => {
                    return (
                        <FlatlistComponent
                            weight={item.weight}
                            isSelected={item.isSelected}
                            source={item.source}
                            title={item.title}
                            price={item.price}
                            onPress={() => {
                                navigation.navigate("AppDeals", { data: item })
                            }} onClick={() => click(item.id)} />
                    )
                }}
            />

            <View style={styles.veg_view}>
                <Text style={GlobalStyles.GroceryText}>{en.GROCERY_APP_DEALS}</Text>
                <Text style={[styles.text_viewAll, { marginLeft: wp(37) }]}>{en.VIEW_ALL}</Text>
                <Image source={imagePaths.smallArrow_img} style={styles.image_arrow}></Image>
            </View>

            <FlatList
                data={fruits}
                horizontal
                renderItem={({ item }) => {
                    return (
                        <FlatlistComponent
                            weight={item.weight}
                            isSelected={item.isSelected}
                            source={item.source}
                            title={item.title}
                            price={item.price}
                            onPress={() => {
                                navigation.navigate("AppDeals", { data: item })
                            }}
                            onClick={() => clickOn(item.id)} />
                    )
                }}
            />
        </View>
    )
}
export default HomeScreen;

const styles = StyleSheet.create({
    home_first_view: {
        alignItems: 'center'
    },
    home_image_view: {
        height: wp(18),
        width: wp(18),
        backgroundColor: colors.SECONDARY_GREEN,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: wp(2)
    },
    home_image: {
        height: wp(5),
        width: wp(5)
    },
    home_text: {
        marginBottom: wp(5),
        marginTop: wp(2),
        fontSize: RFValue(12),
        textAlign: "center",
        fontFamily: fonts.ROBOTO_REGULAR
    },
    down_image_view: {
        alignSelf: 'center',
        marginTop: wp(3)
    },
    down_image: {
        height: hp(1),
        width: wp(10),
    },
    veg_view: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    text_viewAll: {
        marginVertical: hp(1.5),
        marginLeft: wp(20),
        fontSize: RFValue(12),
        color: colors.SECONDARY_GREY,
        fontFamily: fonts.ROBOTO_REGULAR
    },
    image_arrow: {
        marginVertical: hp(2),
        marginRight: wp(5)
    }
})