import { View, Text, StyleSheet, Image, FlatList, ActivityIndicator, RefreshControl } from 'react-native'
import React, { useEffect, useState } from 'react'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFValue } from "react-native-responsive-fontsize";
import { fonts } from '../utils/fonts';
import { useSelector, useDispatch } from 'react-redux';
import { getApiData } from '../redux/action/action';
import { colors } from '../utils/colors';

const ProfileScreen = () => {
  // const myState = useSelector((state) => state.changeNumber)
  // const dispatch = useDispatch();
  const [page, setPage] = useState(1);
  const [refresh, setRefresh] = useState(false);
  const { data, loading } = useSelector((state) => state.apiReducer)
  const dispatch = useDispatch();
  const fetchAPIData = () => dispatch(getApiData("https://rickandmortyapi.com/api/character/?page="+ page));

  useEffect(() => {
    setTimeout(() => {
      fetchAPIData()
    }, 2000)
  }, [])

  const getData = (page) => {
    console.log(data.info);
    if (data.info.next) {
      setPage(page)
      setRefresh(false)
      console.log({ loading });
      console.log(page);
      dispatch(getApiData("https://rickandmortyapi.com/api/character/?page="+ page))
    }
  }

  const onRefreshData = () => {
    setRefresh(true)
    getData(page + 1)
  }

  const renderFooter = () => {
    return (
      <View style={styles.footer}>
        {loading ? (
          <ActivityIndicator color={colors.GREEN} style={{ margin: 15, justifyContent: "center" }} />
        ) : null}
      </View>
    );
  };

  console.log({ data });
  return (
    <View style={styles.mainView}>
        <FlatList
          data={data.data}
          ListEmptyComponent={()=> (
            <ActivityIndicator color={colors.GREEN} style={{ flex: 1, justifyContent: "center" }} />
          )}
          ItemSeparatorComponent={() => {
            return (<View style={styles.item_separator} />);
          }}
          renderItem={({ item }) => {
            return (
              <View style={{ flexDirection: 'row' }}>
                <View>
                  <Image source={{ uri: item.image }} style={styles.flatImage}></Image>
                </View>
                <View style={{ margin: wp(2) }}>
                  <Text>{item.id}</Text>
                  <Text style={styles.flatText}>Name: {item.name}</Text>
                  <Text>Gender: {item.gender}</Text>
                  <Text>Status: {item.status}</Text>
                </View>
              </View>
            )
          }}
          ListFooterComponent={renderFooter}
          onEndReached={() => getData(page + 1)}
          onEndReachedThreshold={0.5}
          refreshControl={
            <RefreshControl
              refreshing={refresh}
              onRefresh={onRefreshData}
              progressBackgroundColor={colors.GREEN}
              tintColor={colors.GREEN}
            />
          }
        />
      
    </View>
  )
}
export default ProfileScreen;

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: 'white',
  },
  item_separator: {
    height: 2,
    backgroundColor: colors.LIGHT_GREY
  },
  flatText: {
    fontFamily: fonts.ROBOTO_MEDIUM,
    fontSize: RFValue(15)
  },
  flatImage: {
    height: hp(10),
    width: wp(20)
  },
  footer: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
})