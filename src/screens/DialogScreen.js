import { useNavigation } from "@react-navigation/native";
import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import ButtonComponent from "../component/ButtonComponent";
import { GlobalStyles } from "../styles/style";
import { en } from "../utils/en";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFValue } from "react-native-responsive-fontsize";
import { colors } from "../utils/colors";
import { imagePaths } from "../utils/imagePaths";
import { fonts } from "../utils/fonts";

const DialogScreen = () => {
    const navigation = useNavigation();
    return (
        <View style={[GlobalStyles.container, styleSheet.container]}>
            <View style={{ flex: 1 }}>
                <Image source={imagePaths.splash_img} style={GlobalStyles.logo_image} />
                <View style={styleSheet.firstView}>
                    <View style={styleSheet.secondView}>
                        <View style={styleSheet.dialogView}>
                            <Image source={imagePaths.dialog_img} style={styleSheet.dialog_image}></Image>
                            <View style={{ marginRight: wp(20) }}>
                                <Text style={styleSheet.Maintext}>
                                    {en.DIALOG}
                                </Text>
                            </View>
                        </View>
                        <Text style={styleSheet.text}>
                            {en.DIALOG_SECOND}
                        </Text>
                        <View style={styleSheet.thirdView}>
                            <TouchableOpacity
                                style={styleSheet.later_btn}
                            >
                                <Text style={styleSheet.later_text}>{en.LATER_TEXT}</Text>
                            </TouchableOpacity>
                            <View style={styleSheet.signin_btn}>
                                <ButtonComponent name={en.SIGNIN_BUTTON} onPress={() => navigation.navigate('SignIn')} />
                            </View>
                        </View>
                    </View>
                </View>

            </View>
        </View>


    )
}
export default DialogScreen;

const styleSheet = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    firstView: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    secondView: {
        height: hp(30),
        backgroundColor: 'white',
        borderRadius: 10,
        marginBottom: 0,
    },
    dialogView: {
        marginTop: wp(5),
        marginLeft: wp(-5),
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-evenly",
        alignItems: 'center'
    },
    Maintext: {
        fontSize: RFValue(24),
        alignSelf: 'center',
        fontFamily: fonts.ROBOTO_BOLD
    },
    image: {
        marginTop: wp(3),
        height: hp(4),
        width: wp(8),
        marginLeft: wp(5)
    },
    text: {
        fontSize: 20,
        color: colors.GREY,
        marginTop: wp(2),
        marginLeft: wp(20),
    },
    thirdView: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: wp(2),
        marginLeft: wp(20),
        marginBottom: wp(10),
        alignItems: 'center'
    },
    later_btn: {
        marginLeft: wp(3),
        marginTop: wp(2)
    },
    later_text: {
        fontSize: RFValue(16),
        color: colors.GREEN,
        marginLeft: wp(20),
        fontFamily: fonts.ROBOTO_REGULAR
    },
    signin_btn: {
        marginLeft: wp(10),
        marginRight: wp(10)
    },
    dialog_image: {
        height: wp(10),
        width: wp(10),
        marginTop: wp(4)
    }
})