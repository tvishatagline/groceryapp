import React, { useState } from "react";
import { Text, TouchableOpacity, View, TextInput, StyleSheet, SafeAreaView } from "react-native";
import { en } from "../utils/en";
import { useNavigation } from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import ButtonComponent from "../component/ButtonComponent";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { GoogleSignin, GoogleSigninButton, statusCodes } from '@react-native-google-signin/google-signin';
import { GlobalStyles } from "../styles/style";
import { colors } from "../utils/colors";
import { LoginButton, AccessToken, GraphRequest, GraphRequestManager } from "react-native-fbsdk";

const SignInScreen = () => {
  const navigation = useNavigation();
  const [emailAddress, setEmailAddress] = useState('');
  const [userPassword, setUserPassword] = useState('');
  const [emailError, setEmailError] = useState(null);
  const [passwordError, setPasswordError] = useState(null);

  var emailValid = false;
  var passValid = false;

  const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  const emailValidation = () => {
    if (emailAddress.length == 0) {
      setEmailError("Email is required");
    } else if (reg.test(emailAddress) == false) {
      setEmailError("Invalid Email Address");
    } else {
      setEmailError(null);
      emailValid = true;
    }
  }

  const passValidation = () => {
    if (userPassword.length == 0) {
      setPasswordError("Password is required");
    } else if (userPassword.length < 6) {
      setPasswordError("Password should be minimum 6 characters!");
    } else if (userPassword.length > 15) {
      setPasswordError("Password should not maximum 15 characters!");
    } else if (userPassword.indexOf(' ') >= 0) {
      setPasswordError("Password can not contain spaces!");
    } else {
      setPasswordError(null);
      passValid = true;
    }
  }

  const signinValidation = async () => {
    emailValidation();
    passValidation();
    try {
      if (passValid && emailValid) {
        await AsyncStorage.setItem('SignupEmail', emailAddress);
        await AsyncStorage.setItem('SignupPass', userPassword);
        // navigation.dispatch(
        //     StackActions.replace('DrawerStack')
        // );
        navigation.navigate('DrawerStack')
        // navigation.reset({
        //     index: 0,
        //     routes: [{ name: 'DrawerStack' }]
        // })
      }
    } catch {
      console.error(e);
    }
  }

  const signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices().then(async (user) => {
        if (user) {
          await GoogleSignin.signIn().then((user1) => {
            console.log({ user1 });
            navigation.navigate('DrawerStack')
          });
        }
      });

    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('Users Cancelled the login flow');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('Signing In');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('Play Services Not Available or Outdated');
      }
      else {
        console.log('Some Other Error Happend', error);
        console.log({ error })
      }
    }
  };

  const getResponseInfo = (error, result) => {
    console.log("first", { error, result })
    try {
      if (error) {
        console.log("error==>")
        alert('Error fetching data: ' + error.toString());
      } else {
        console.log(JSON.stringify(result));
        navigation.navigate('DrawerStack')
      }
    } catch (error) {
      console.log('error', error)
    }
  };

  return (
    <SafeAreaView edges={['top']} style={GlobalStyles.safeAreaView}>
      <View style={GlobalStyles.shadowView}>
        <View style={GlobalStyles.shadowTopBtnView}>
          <TouchableOpacity onPress={() => navigation.navigate('SignIn')}>
            <Text style={[GlobalStyles.shadowTopBtnText, {
              color: colors.GREEN
            }]}>{en.FRONT_SIGNIN}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('SignUp')}>
            <Text style={GlobalStyles.shadowTopBtnText}>{en.FRONT_SIGNUP}</Text>
          </TouchableOpacity>
        </View>
        <Text style={GlobalStyles.titleText}>{en.TEXT_SIGNIN}</Text>
        <Text style={GlobalStyles.subtitleText}>{en.TEXT_SIGNIN_START}</Text>
        <TextInput
          style={GlobalStyles.TextInput}
          placeholder="Email address"
          value={emailAddress}
          onChangeText={(data) => {
            setEmailAddress(data);
            emailValidation();
          }}
          onBlur={emailValidation}
        />
        {emailError &&
          <Text style={GlobalStyles.errorMessage}>{emailError}</Text>
        }
        <TextInput
          style={GlobalStyles.TextInput}
          placeholder="Password"
          secureTextEntry={true}
          value={userPassword}
          onChangeText={(data) => {
            setUserPassword(data);
            passValidation();
          }}
          onBlur={passValidation}
        />
        {passwordError &&
          <Text style={GlobalStyles.errorMessage}>{passwordError}</Text>
        }
        <View style={styles.signInView}>
          <TouchableOpacity onPress={() => navigation.navigate('ForgotPass')}>
            <Text style={GlobalStyles.subtitleText}>
              Forgot password?
            </Text>
          </TouchableOpacity>
          <ButtonComponent name={en.SIGNIN_BUTTON} onPress={() => signinValidation()} />
        </View>
        <GoogleSigninButton
          style={styles.googleSignInButton}
          size={GoogleSigninButton.Size.Wide}
          color={GoogleSigninButton.Color.Dark} onPress={() => signIn()}>
        </GoogleSigninButton>

        <LoginButton
          readPermissions={['public_profile', 'email']}
          style={styles.facebookButton}
          onLoginFinished={(error, result) => {
            console.log({ error, result })
            try {
              if (error) {
                console.log('Login has error: ' + result.error);
              } else if (result.isCancelled) {
                console.log('Login is cancelled.')
              } else {
                try {
                  AccessToken.getCurrentAccessToken().then((data) => {
                    console.log({ data });
                    const processRequest = new GraphRequest(
                      '/me?fields=name,picture.type(large)',
                      null,
                      getResponseInfo,
                    );
                    console.log(processRequest)
                    new GraphRequestManager()
                      .addRequest(processRequest).start();
                  });
                } catch (error) {
                  console.log("first", error)
                }

              }
            } catch (error) {
              console.log(error);
            }

          }}
          onLogoutFinished={() => console.log("Logout")}
        />


      </View>
    </SafeAreaView>
  );
}

export default SignInScreen;

const styles = StyleSheet.create({
  googleSignInButton: {
    width: wp(50),
    height: hp(6),
    alignSelf: 'flex-end',
  },
  signInView: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginTop: hp(1.5),
  },
  btnNormal: {
    borderColor: 'blue',
    borderWidth: 1,
    borderRadius: 10,
    height: 30,
    width: 100,
  },
  btnPress: {
    borderColor: 'blue',
    borderWidth: 1,
    height: 30,
    width: 100,
  },
  facebookButton: {
    marginTop: wp(2),
    width: wp(50),
    height: hp(5),
    alignSelf: 'flex-end',
  }
})