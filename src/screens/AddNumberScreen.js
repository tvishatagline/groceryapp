import React, { useRef, useState } from 'react';
import { Alert, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import PhoneInput from 'react-native-phone-number-input';
import { en } from '../utils/en';
import { GlobalStyles } from "../styles/style";
import { useNavigation } from '@react-navigation/native';
import ButtonComponent from '../component/ButtonComponent';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const AddNumberScreen = () => {
    const navigation = useNavigation();
    const [phoneNumber, setPhoneNumber] = useState('');
    const [formattedValue, setFormattedValue] = useState("");
    const phoneInput = useRef(null);
    const getPhoneNumber = () => {
        console.log({ phoneNumber });
        Alert.alert(phoneNumber);
    };

    return (
        <SafeAreaView style={GlobalStyles.safeAreaView}>
            <View style={GlobalStyles.shadowView}>
                <Text style={GlobalStyles.titleText}>{en.ADD_NUMBER_TEXT}</Text>
                <Text style={GlobalStyles.subtitleText}>{en.ENTER_MOBILE_CONTINUE}</Text>
                <PhoneInput
                    ref={phoneInput}
                    defaultValue={phoneNumber}
                    onChangeText={text => {
                        setPhoneNumber(text);
                    }

                    }
                    onChangeFormattedText={text => {
                        setFormattedValue(text);
                    }}
                    containerStyle={styleSheet.phoneNumberView}
                    textContainerStyle={{ paddingVertical: 0 }}
                    layout="first"
                    autoFocus
                />
                <View style={styleSheet.number_btn}>
                    <ButtonComponent name="SEND" onPress={() => navigation.navigate("VerifyNumber")} />
                </View>
            </View>
        </SafeAreaView>
    )

}

export default AddNumberScreen;

const styleSheet = StyleSheet.create({

    phoneNumberView: {
        width: wp(90),
        height: wp(12),
        backgroundColor: 'white',   
    }, 
    number_btn: {
        alignItems: 'flex-end',
       marginTop:hp(3)
    }

});