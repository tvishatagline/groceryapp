import { useNavigation } from "@react-navigation/native";
import React from "react";
import { SafeAreaView, Text, TextInput, View } from "react-native";
import ButtonComponent from "../component/ButtonComponent";
import { GlobalStyles } from "../styles/style";
import { en } from "../utils/en";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const ForgotPassScreen = () => {
    const navigation = useNavigation();
    return (
        <SafeAreaView style={GlobalStyles.safeAreaView}>
            <View style={GlobalStyles.shadowView}>
                <Text style={GlobalStyles.titleText}>{en.FORGOT_PASSWORD}</Text>
                <Text style={GlobalStyles.subtitleText}>Enter email address to send reset code</Text>
                <TextInput
                    style={GlobalStyles.TextInput}
                    placeholder="Email address"
                    type='email'
                />
                <View style={{ alignItems: 'flex-end', margin: wp(5) }}>
                    <ButtonComponent name="SEND" />
                </View>
            </View>
        </SafeAreaView>
    )
}

export default ForgotPassScreen;
