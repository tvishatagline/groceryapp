import React, { useState, useEffect } from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { colors } from '../utils/colors';
import { Appbar } from "react-native-paper";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { GlobalStyles } from '../styles/style';
import { useNavigation } from '@react-navigation/native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AddToCart from '../component/cartItemComponent';
import { RFValue } from 'react-native-responsive-fontsize';
import { fonts } from '../utils/fonts';

const AddToCartScreen = () => {
  const navigation = useNavigation();
  const [data, setData] = useState([]);
  const [price, setPrice] = useState(false);
  const [itemPrice, setItemPrice] = useState()


  useEffect(() => {
    (async () => {
      getDataFu()
    })()
  }, [])

  const getDataFu = async () => {
    const cartData = await AsyncStorage.getItem('cartData')
    console.log("CartData--->", JSON.parse(cartData))
    setData(JSON.parse(cartData))
    totalAmount(JSON.parse(cartData))
  }

  const deleteItem = async (id) => {
    const cartData = await AsyncStorage.getItem('cartData')
    const storeData = JSON.parse(cartData)
    let data = [...storeData]
    console.log("Function", data);

    const newData = data.filter(item => item.id !== id)
    setData(newData)
    console.log('NewData', newData);
    await AsyncStorage.setItem('cartData', JSON.stringify((newData.length === 0) ? null : newData))
  }

  const totalAmount = (data) => {
    console.log("sv", data);
    let initialValue = 0
    let totalAmount = data.reduce((preValue, currValue) => preValue + (currValue.price * currValue.quantity), initialValue);
    console.log("keh", totalAmount);
    setItemPrice(totalAmount)
  }

  const updatedData = (data) => {
    console.log("aSdf", data);
    totalAmount(data)
  }

  return (
    <View style={{ flex: 1 }}>
      <SafeAreaView edges={['top']} style={styles.safeAreaView}>
        <View style={GlobalStyles.appBarStyle}>
          <View style={styles.appIconClose}>
            <Appbar.Action icon='close' color="white" onPress={() => navigation.navigate('TabNavigation')} />
            <Text style={GlobalStyles.appBarText}>AddToCart</Text>
          </View>
          <View style={styles.notificationIcon}>
            <Appbar.Action icon="cart" color="white" onPress={() => navigation.navigate('AddToCart')} />
          </View>
        </View>
      </SafeAreaView>
      <FlatList
        data={data}
        ItemSeparatorComponent={(props) => {
          return (<View style={styles.item_separator} />);
        }}
        renderItem={({ item, index }) => {
          return (
            <AddToCart item={item} index={index} onPress={() => {
              deleteItem(item.id);
            }}
              increment={() => setPrice(true)}
              decrement={() => setPrice(false)}
              newData={(data) => updatedData(data)}
            />
          )
        }}
      />
      <View style={styles.bottomView}>
        <Text style={styles.bottomViewText}>Total Item:{data?.length}</Text>
        <Text style={styles.bottomViewText}>Total Amount:{itemPrice}.00</Text>
      </View>

    </View>
  );
};

export default AddToCartScreen;

const styles = StyleSheet.create({
  appIconClose: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  safeAreaView: {
    backgroundColor: colors.GREEN
  },
  item_separator: {
    height: 2,
    backgroundColor: colors.LIGHT_GREYs
  },
  button: {
    height: wp(7),
    width: wp(7),
    backgroundColor: colors.GREY,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center'
  },
  bottomViewText: {
    color: colors.WHITE,
    fontSize: RFValue(18),
    fontFamily: fonts.ROBOTO_REGULAR
  },
  bottomView: {
    backgroundColor: colors.GREEN,
    height: hp(10),
    bottom: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: wp(5)
  }
})