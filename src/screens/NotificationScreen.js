import { View, Text, SafeAreaView, FlatList, Image, ScrollView, StyleSheet } from 'react-native'
import React from 'react'
import { Appbar } from "react-native-paper";
import { useNavigation } from '@react-navigation/native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFValue } from "react-native-responsive-fontsize";
import { colors } from '../utils/colors';
import { imagePaths } from '../utils/imagePaths';
import { GlobalStyles } from '../styles/style';
import { fonts } from '../utils/fonts';

const notificationDATA = [
  {
    id: 1,
    source: imagePaths.notification_img,
    title: 'Fresh Food Fiesta',
    label: 'New',
    desc: '23rd - 30th November on selected\nVegitables, Fruits, Fish & Meats.'
  }, {
    id: 2,
    source: imagePaths.notification_img,
    title: 'Nexus Promotion',
    label: '4 min ago',
    desc: 'Kraft Cheese Tin 200G was: Rs.825.00\nNexus Member Deals: Rs.618.00.'
  }
];

const notificationDATA1 = [
  {
    id: 1,
    source: imagePaths.notification_img,
    title: 'Fresh Food Fiesta',
    label: '10:14 AM',
    desc: 'Lorem ipsum dolor sit amet,\nconsecteture adipiscing elit sed'
  },
];

const notificationDATA2 = [
  {
    id: 1,
    source: imagePaths.notification_img,
    title: 'Fresh Food Fiesta',
    label: 'Mon.10:14AM',
    desc: 'Lorem ipsum diam nonum Lorem,\niqsum dolor sit amet, consectetuer'
  }, {
    id: 2,
    source: imagePaths.notification_img,
    title: 'Food Promotion',
    label: 'Sun.2.20PM',
    desc: 'Adipiscing elit, sed diam nonum\nLorem ipsum dolor sit amet, consec'
  }

];

const NotificationScreen = () => {

  const navigation = useNavigation();
  return (
    <View>
      <SafeAreaView edges={['top']} style={styles.safeAreaView}>
        <View style={GlobalStyles.appBarStyle}>
          <View style={styles.appIconClose}>
            <Appbar.Action icon='close' color="white" onPress={() => navigation.navigate('TabNavigation')} />
            <Text style={GlobalStyles.appBarText}>Notification</Text>
          </View>
          <View style={styles.notificationIcon}>
            <Appbar.Action icon="bell" color="white" onPress={() => navigation.navigate('Notification')} />
          </View>
        </View>
      </SafeAreaView>
      <ScrollView>
        <View style={styles.notificationTit}>
          <Text>Today</Text>
          <Text>Clear all</Text>
        </View>

        <View>
          <FlatList
            data={notificationDATA}
            scrollEnabled={false}
            renderItem={({ item }) => {
              return (
                <View >
                  <View style={styles.notification}>
                    <View style={styles.notificationImage}>
                      <Image source={item.source} style={styles.notificationSource} />

                    </View><View>
                      <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.notificationTitle}>{item.title}</Text>
                        <Text style={styles.notificationLabel}>{item.label}</Text></View>
                      <Text style={styles.notificationDesc}>{item.desc}</Text></View>
                  </View>
                </View>
              )
            }}
          />
        </View>

        <View style={styles.notificationTit}>
          <Text>Yesterday</Text>
          <Text>Clear</Text>
        </View>
        <View>
          <FlatList
            data={notificationDATA1}
            scrollEnabled={false}
            renderItem={({ item }) => {
              return (
                <View >
                  <View style={styles.notification}>
                    <View style={styles.notificationImage}>
                      <Image source={item.source} style={styles.notificationSource} />

                    </View><View>
                      <View style={styles.view}>
                        <Text style={styles.notificationTitle}>{item.title}</Text>
                        <Text style={styles.notificationLabel}>{item.label}</Text></View>
                      <Text style={styles.notificationDesc}>{item.desc}</Text></View>
                  </View>
                </View>
              )
            }}
          />
        </View>
        <View style={styles.notificationTit}>
          <Text>Last week</Text>
          <Text>Clear</Text>
        </View>
        <View>
          <FlatList
            data={notificationDATA2}
            scrollEnabled={false}
            renderItem={({ item }) => {
              return (
                <View >
                  <View style={styles.notification}>
                    <View style={styles.notificationImage}>
                      <Image source={item.source} style={styles.notificationSource} />

                    </View><View>
                      <View style={styles.notificationView}>
                        <Text style={styles.notificationTitle}>{item.title}</Text>

                        <Text style={styles.notificationLabel}>{item.label}</Text></View>
                      <Text style={styles.notificationDesc}>{item.desc}</Text></View>
                  </View>
                </View>
              )
            }}
          />
        </View>
      </ScrollView>
    </View>
  )
}
export default NotificationScreen;
const styles = StyleSheet.create({
  notification: {
    flexDirection: 'row',
    backgroundColor: 'white',
    borderRadius: 8,
    height: 110,
    marginTop: hp(1),
    marginLeft: wp(4),
    marginRight: wp(4),
    padding: wp(2),
    shadowColor: colors.GREY,
  },
  notificationSource: {
    height: wp(5),
    width: wp(5)
  },
  notificationImage: {
    height: wp(12),
    width: wp(12),
    backgroundColor: colors.SECONDARY_GREEN,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: wp(2),
    marginTop: wp(2)
  },
  notificationTitle: {
    marginLeft: wp(5),
    marginBottom: wp(4),
    marginTop: wp(2),
    fontSize: RFValue(16),
    fontFamily: fonts.ROBOTO_BOLD
  },
  notificationDesc: {
    marginLeft: 20,
    arginBottom: 5,
    fontSize: RFValue(14),
    fontFamily: fonts.ROBOTO_REGULAR
  },
  notificationTit: {
    flexDirection: 'row',
    fontSize: RFValue(12),
    justifyContent: 'space-between',
    marginLeft: '3%',
    marginRight: '3%',
    marginTop: '3%',
    textAlign: 'center',
    fontFamily: fonts.ROBOTO_REGULAR
  },
  notificationLabel: {
    color: colors.GREEN,
    marginLeft: wp(15),
    marginTop: wp(4),
    fontSize: RFValue(10),
    fontFamily: fonts.ROBOTO_REGULAR
  },
  appIconClose: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  safeAreaView: {
    backgroundColor: colors.GREEN
  },
  notificationIcon: {
    flexDirection: 'row'
  },
  notificationView: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  view: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center'
  }
}) 