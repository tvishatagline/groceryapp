import React, { useState } from 'react'
import { View, Text, SafeAreaView, Image, TouchableOpacity, StyleSheet, Alert, ScrollView } from 'react-native'
import { Appbar } from "react-native-paper";
import { GlobalStyles } from '../styles/style';
import { AirbnbRating } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';
import { colors } from '../utils/colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFValue } from "react-native-responsive-fontsize";
import { imagePaths } from '../utils/imagePaths';
import { fonts } from '../utils/fonts';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { addToCart } from '../utils/cartItem';

const GroceryAppDealsScreen = ({ route }) => {
  const navigation = useNavigation();
  const { data } = route.params;

  const [count, setCount] = useState(1);
  const onIncrement = () => setCount(prevCount => prevCount + 1)
  const onDecrement = () => {
    if (count > 1) {
      setCount(prevCount => prevCount - 1)
    }
  }

  return (
    <ScrollView>
      <SafeAreaView edges={['top']} style={styles.safe_view}>
        <View style={GlobalStyles.appBarStyle}>
          <View style={styles.app_title}>
            <Appbar.BackAction color="white" onPress={() => navigation.navigate("TabNavigation")} />
            <Text style={GlobalStyles.appBarText}>Grocery App Deals</Text>
          </View>
          <Appbar.Action color="white" />
        </View>
      </SafeAreaView>
      <View style={styles.app_deals_view}>
        <Image source={data.source} style={styles.image}></Image>
        <Text style={styles.weight_app_deals}>{data.weight}</Text>
        <Text style={styles.name_app_delas}>{data.title}</Text>
        <Text style={styles.price_app_deals}>{data.price}</Text>
        <AirbnbRating
          reviews={false}
        />
        <Text style={styles.quantity_text}>Quantity</Text>
        <View style={styles.btn_view}>
          <TouchableOpacity onPress={onDecrement}>
            <View style={styles.button}>
              <Image source={imagePaths.minus_img}></Image>
            </View>
          </TouchableOpacity>
          <Text style={styles.counter}>{count}</Text>
          <TouchableOpacity onPress={onIncrement}>
            <View style={styles.button}>
              <Image source={imagePaths.plus_img}></Image>
            </View>
          </TouchableOpacity>
        </View>
        <TouchableOpacity style={styles.btn_buy} onPress={async () => await AsyncStorage.clear()}>
          <Text style={styles.text_buy}>BUY NOW</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.btn_cart} onPress={() => addToCart(data, count, navigation)}>
          <Text style={styles.text_cart}>ADD TO CART</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  )
}

export default GroceryAppDealsScreen;

const styles = StyleSheet.create({
  safe_view: {
    backgroundColor: colors.GREEN
  },
  app_title: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  app_deals_view: {
    alignItems: 'center',
    marginTop: wp(10),
  },
  weight_app_deals: {
    backgroundColor: colors.LIGHT_GREY,
    color: colors.GREY,
    marginTop: wp(5),
    fontSize: RFValue(10),
    padding: wp(1),
    textAlign: 'center',
    fontFamily: fonts.ROBOTO_REGULAR
  },
  name_app_delas: {
    fontSize: RFValue(24),
    marginTop: wp(5),
    fontWeight: '600',
    fontFamily: fonts.ROBOTO_REGULAR
  },
  rating: {
    marginTop: wp(10),
  },
  price_app_deals: {
    color: colors.GREY,
    fontSize: RFValue(16),
    marginTop: wp(2),
    fontFamily: fonts.ROBOTO_REGULAR
  },
  quantity_text: {
    color: colors.GREY,
    fontSize: RFValue(16),
    marginTop: wp(12),
    fontFamily: fonts.ROBOTO_REGULAR
  },
  btn_view: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: wp(70),
    marginTop: wp(2)
  },
  button: {
    height: wp(15),
    width: wp(15),
    backgroundColor: colors.GREY,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center'
  },
  counter: {
    fontSize: RFValue(30),
    fontFamily: fonts.ROBOTO_BOLD
  },
  btn_buy: {
    backgroundColor: colors.GREEN,
    height: wp(12),
    width: wp(40),
    borderRadius: 5,
    fontSize: RFValue(14),
    alignItems: 'center',
    marginTop: wp(5),
    padding: wp(2),
    fontFamily: fonts.ROBOTO_REGULAR
  },
  text_buy: {
    color: 'white',
    padding: wp(2)
  },
  btn_cart: {
    height: wp(12),
    width: wp(40),
    borderRadius: 5,
    fontSize: RFValue(14),
    alignItems: 'center',
    marginTop: wp(5),
    padding: wp(2),
    fontFamily: fonts.ROBOTO_REGULAR
  },
  text_cart: {
    color: colors.GREEN,
    padding: wp(2)
  },
  image: {
    width: wp(60),
    height: wp(40),
    resizeMode: 'contain'
  }
})