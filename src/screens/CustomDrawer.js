
import React from "react";
import { FlatList, View, Text, Image, StyleSheet } from "react-native";
import { colors } from "../utils/colors";
import { imagePaths } from "../utils/imagePaths";

const DATA = [
    {
        id: "1",
        source: imagePaths.custom_drawer_img_first,
        title: 'Order History'

    },
    {
        id: "2",
        source: imagePaths.custom_drawer_img_second,
        title: 'Track Orders'
    },
    {
        id: "3",
        source: imagePaths.custom_drawer_img_third,
        title: 'Currency'

    },
    {
        id: "4",
        source: imagePaths.custom_drawer_img_forth,
        title: 'Store Locator'
    },
    {
        id: "5",
        source: imagePaths.custom_drawer_img_fifth,
        title: 'Terms & Conditions'

    },
    {
        id: "6",
        source: imagePaths.custom_drawer_img_sixth,
        title: 'Help'
    },
    {
        id: "7",
        source: imagePaths.custom_drawer_img_seventh,
        title: 'Got a Questions?'

    },
    {
        id: "8",
        source: imagePaths.custom_drawer_img_eighth,
        title: 'Logout'

    },
];
const CustomDrawer = () => {
    return (
        <View style={styleSheet.mainView}>
            <View style={styleSheet.sec_view}>
                <Image source={imagePaths.close_img} style={styleSheet.imageDrawer}></Image>
                <Text style={styleSheet.text_Drawer}>Grocery Shopping</Text></View>
            <FlatList
                data={DATA}
                renderItem={({ item }) => {
                    return (
                        <View style={styleSheet.flat_view}>
                            <View style={styleSheet.flat_sec_view}>
                                <View style={styleSheet.image_view}>
                                    <Image source={item.source} style={styleSheet.drawer_image} />
                                </View>
                                <Text style={styleSheet.drawer_text}>{item.title}</Text>
                            </View>
                        </View>
                    )
                }}
            />
        </View>
    )
}
export default CustomDrawer;

const styleSheet = StyleSheet.create({
    mainView: {
        marginTop: '15%',
        backgroundColor: 'white',
        height: 700,
        borderRadius: 20
    },
    sec_view: {
        flexDirection: 'row',
        marginTop: 20
    },
    imageDrawer: {
        height: 20,
        width: 20,
        marginLeft: '13%'
    },
    text_Drawer: {
        marginLeft: '10%'
    },
    flat_view: {
        marginTop: '5%'
    },
    flat_sec_view: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    image_view: {
        height: 60,
        width: 60,
        backgroundColor: colors.SECONDARY_GREEN,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: '8%'
    },
    drawer_image: {
        height: 20,
        width: 20
    },
    drawer_text: {
        marginLeft: '5%'
    }
})