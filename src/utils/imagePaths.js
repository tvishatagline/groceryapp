export const imagePaths = {
    splash_img: require("../assets/Splash.png"),
    dialog_img: require("../assets/tick.png"),
    arrow_img: require('../assets/arrow.png'),
    magnify_img: require('../assets/Magnify.png'),
    smallArrow_img: require("../assets/smallArrow.png"),
    coffee_img_first: require("../assets/Coffee1.png"),
    coffee_img_second: require("../assets/Coffee2.png"),
    coffee_img_third: require("../assets/Coffee3.png"),
    coffee_img_forth: require("../assets/Coffee4.png"),
    coffee_img_fifth: require("../assets/Coffee5.png"),
    custom_drawer_img_first: require("../assets/Path.png"),
    custom_drawer_img_second: require("../assets/Path1.png"),
    custom_drawer_img_third: require("../assets/Path2.png"),
    custom_drawer_img_forth: require("../assets/Path3.png"),
    custom_drawer_img_fifth: require("../assets/Path4.png"),
    custom_drawer_img_sixth: require("../assets/Path5.png"),
    custom_drawer_img_seventh: require("../assets/Path6.png"),
    custom_drawer_img_eighth: require("../assets/Path7.png"),
    close_img: require("../assets/icon.png"),
    chilli_img: require("../assets/Chilli.png"),
    minus_img: require("../assets/minus.png"),
    plus_img: require("../assets/plus.png"),
    house_hold_img: require("../assets/Household.png"),
    grocery_img: require("../assets/Grocery.png"),
    liquor_img: require("../assets/Liquor.png"),
    chillied_img: require("../assets/Chilled.png"),
    beverages_img: require("../assets/Beverages.png"),
    pharmacy_img: require("../assets/Pharmacy.png"),
    frozen_food_img: require("../assets/Frozenfood.png"),
    vegetables_img: require("../assets/Vegetables.png"),
    meat_img: require("../assets/Meat.png"),
    fish_img: require("../assets/Fish.png"),
    home_ware_img: require("../assets/Homeware.png"),
    fruits_img: require("../assets/Fruits.png"),
    carrot_img: require("../assets/Carrot.png"),
    mango_img: require("../assets/Mango.png"),
    grapes_img: require("../assets/Grapes.png"),
    ginger_img: require("../assets/Ginger.png"),
    garlic_img: require("../assets/Garlic.png"),
    onion_img: require("../assets/Onion.png"),
    down_arrow_img: require("../assets/downArrow.png"),
    filled_img: require("../assets/Filled.png"),
    without_filled_img: require("../assets/Heart.png"),
    notification_img: require("../assets/Notification.png"),
    search_img: require("../assets/Search.png"),
    cold_dirnks_img: require("../assets/image.png"),

}