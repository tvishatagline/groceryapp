export const fonts = {
    ROBOTO_BOLD: 'Roboto-Bold',
    ROBOTO_MEDIUM: 'Roboto-Medium',
    ROBOTO_REGULAR:'Roboto-Regular',
    ROBOTO_LIGHT:'Roboto-Light'   
}