import AsyncStorage from "@react-native-async-storage/async-storage";
import { Alert } from "react-native";

export const addToCart = async (data, count, navigation) => {
    try {
        const cartData = await AsyncStorage.getItem('cartData')
        const parseCartData = JSON.parse(cartData)
        if (parseCartData) {
            let cloneData = [...parseCartData]
            if (data.categories === cloneData[0]?.categories) {
                const elementIndex = cloneData.findIndex((obj) => obj.id === data.id);
                if (elementIndex !== -1) {
                    cloneData[elementIndex].quantity = cloneData[elementIndex].quantity + count;
                    Alert.alert("Data Updated Successfully")
                    navigation.navigate('AddToCart')
                } else {
                    cloneData.push({ ...data, quantity: count })
                    navigation.navigate('AddToCart')
                }
                await AsyncStorage.setItem('cartData', JSON.stringify(cloneData))
            } else {
                Alert.alert("Please Add Same Category")
            }
        } else {
            let cartArray = [{ ...data, quantity: count }]
            await AsyncStorage.setItem('cartData', JSON.stringify(cartArray))
            Alert.alert(("Data added Successfully"))
            navigation.navigate('AddToCart')
        }
    } catch (e) {
        console.error(e)
    }

}
